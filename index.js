"use strict";

const { strict } = require('assert');
const cookie = require('cookie');
var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const { uniqueNamesGenerator, adjectives, colors, animals } = require('unique-names-generator');
var path = require('path');

var messageHistory = [];
var userNameList = [];
var userList = new Map(); //SocketID -> Details {Username: , Colour: }

function addToMessageHistory(message) {

    if (messageHistory.length >= 200) {
        messageHistory.pop();
    }
    messageHistory.unshift(message);
}

function newMessage(payload, socket) {


    var date = new Date;
    var timestamp = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

    var idJSON = userList.get(socket.id);
    var newChat = {
        type: "chat",
        socketID: socket.id,
        userName: idJSON.UserName,
        userColor: idJSON.Colour,
        message: payload,
        timestamp: timestamp,
    }
    addToMessageHistory(newChat);

    return newChat;
}

function checkCommand(payload) {
    var check = payload.split(" ");
    if (check[0][0] === "/") {
        if (check[0] === "/name") {
            return 'Name Change';
        }
        if (check[0] === "/color") {
            return 'Color Change'
        }
    }
    return 'null';
}

function setUsername(UserName, socket, callback) {



    if (userNameList.includes(UserName)) {
        socket.emit("Error", "UserName \"" + UserName + "\" is already taken");
        // if (typeof callback == "function") callback();
        return true;
    }
    else if (!userList.has(socket.id)) {

        userNameList.push(UserName);
        userList.set(socket.id, { UserName: UserName, Color: "#000000" });
        var date = new Date;
        var timestamp = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        var nameChange = {
            type: "Set Name",
            newName: UserName,
            timestamp: timestamp,
        }
        addToMessageHistory(nameChange);
        socket.emit("Update Username", nameChange.newName, messageHistory);
        io.emit('Update Message Log', messageHistory);
        if (typeof callback == "function") callback();
        return true;

    }
    else {
        var userInformation = userList.get(socket.id);
        var date = new Date;
        var timestamp = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        var nameChange = {
            type: "Name Update",
            newName: UserName,
            oldName: userInformation.UserName,
            timestamp: timestamp,
        }
        var index = userNameList.indexOf(nameChange.oldName);
        userNameList[index] = nameChange.newName;
        userList.set(socket.id, { UserName: UserName });
        //Change all names in the message history
        updateHistoryUsernames(nameChange.newName, socket);
        addToMessageHistory(nameChange);
        socket.emit("Update Username", nameChange.newName, messageHistory);
        io.emit("Update User List", userNameList);
        io.emit('Update Message Log', messageHistory);
        if (typeof callback == "function") callback();
        return true;
    }


}

function setRandomUsername(socket, callback) {
    const randomName = uniqueNamesGenerator({ dictionaries: [adjectives, colors, animals], style: "capital", separator: "" });
    setUsername(randomName, socket);
    callback();
}

function updateHistoryUsernames(newUserName, socket) {
    messageHistory.forEach(message => {
        if (message.socketID === socket.id) {
            message.userName = newUserName;
        }
    });
}

function updateHistoryUsercolor(newUsercolor, socket) {
    messageHistory.forEach(message => {
        if (message.socketID === socket.id) {
            message.userColor = newUsercolor;
        }
    });
}

function isValidColor(hex) {
    return typeof hex === 'string'
        && hex.length === 6
        && !isNaN(Number('0x' + hex))
}

function setColor(newColor, socket, callback) {
    if (isValidColor(newColor)) {

        var NewColor = "#" + newColor
        var idJSON = userList.get(socket.id);
        // console.log(idJSON);
        var date = new Date;
        var timestamp = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        var colorChange = {
            type: "Color Change",
            userName: idJSON.UserName,
            color: NewColor,
            timestamp: timestamp,
        }

        var oldID = userList.get(socket.id);
        var newId = {
            UserName: oldID.UserName,
            Colour: NewColor,
        }

        userList.set(socket.id, newId);
        updateHistoryUsercolor(colorChange.color, socket);
        addToMessageHistory(colorChange);
        socket.emit("Update Colour", colorChange.color, messageHistory);
        if (typeof callback == "function") callback();
    }
    else {
        socket.emit("Error", "\"" + newColor + "\" is not a valid hex colour");
    }
}

function setInitialValues(socket, cookies) {


    if (cookies.username != 'undefined' && cookies.username != null && !userNameList.includes(cookies.username)) {
        //cookie exists, and name is not already taken
        setUsername(cookies.username, socket, () => {
            var newId = {
                UserName: cookies.username,
                Colour: cookies.color,
            }
            userList.set(socket.id, newId);
        });

    }
    else {
        //New User
        setRandomUsername(socket, () => {
            var oldID = userList.get(socket.id);
            var newId = {
                UserName: oldID.UserName,
                Colour: '#000000',
            }
            userList.set(socket.id, newId);
        });
        socket.emit('setColorCookie', '#000000');
    }

}

function removeUser(socket) {
    var removedUser = userList.get(socket.id);
    userList.delete(socket.id);
    var deleteIndex = userNameList.indexOf(removedUser.UserName);
    userNameList.splice(deleteIndex, 1);
    io.emit("Update User List", userNameList);
    var date = new Date;
    var timestamp = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    var disconnect = {
        type: "Disconnect",
        userName: removedUser.UserName,
        timestamp: timestamp,
    }
    addToMessageHistory(disconnect);
    io.emit('Update Message Log', messageHistory);

}

app.use(express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});



io.on('connection', (socket) => {


    const cookies = cookie.parse(socket.request.headers.cookie || '');
    console.log(cookies);
    setInitialValues(socket, cookies);
    // console.log("New Connection " + socket.id);
    io.emit("Update User List", userNameList);
    socket.emit("New Connection", messageHistory);


    socket.on('message', (res) => {

        var payload = res.trim();
        var commandType = checkCommand(payload);

        if (commandType === 'Name Change') {
            // console.log("Name Change");
            var command = payload.split(" ");
            setUsername(command[1], socket, () => {
                io.emit('Update Message Log', messageHistory);
            });
        }
        else if (commandType === 'Color Change') {
            // console.log("Color Change");
            var command = payload.split(" ");
            setColor(command[1], socket, () => {
                io.emit('Update Message Log', messageHistory);
            })
        }
        else {
            newMessage(payload, socket);
            io.emit('Update Message Log', messageHistory);
        }
    });
    socket.on('disconnect', () => {
        // console.log(socket.id + "has disconnected");
        removeUser(socket);
    });
});



http.listen(3000, () => {
    console.log('listening on *:3000');
});
