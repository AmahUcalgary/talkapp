"use strict"

var myUserName = "Test";
var myColor = "#000000";

function proccessChat(rawChat) {
    rawChat = rawChat.replaceAll(":)", "&#128516");
    rawChat = rawChat.replaceAll(":(", "&#128542");
    rawChat = rawChat.replaceAll(":o", "&#128558");
    rawChat = rawChat.replaceAll("Fuck", "#$!%");
    rawChat = rawChat.replaceAll("fuck", "#$!%");
    rawChat = rawChat.replaceAll("FUCK", "#$!%");
    rawChat = rawChat.replaceAll("shit", "#$!%");
    rawChat = rawChat.replaceAll("SHIT", "#$!%");
    rawChat = rawChat.replaceAll("Shit", "#$!%");

    return rawChat;
}

function updateUserList(userList) {

    var users = "";

    userList.forEach(userName => {
        var you = "";
        var bold = "";
        var boldEnd = "";
        if (userName === myUserName) {
            you = "You: "
            bold = "<strong>"
            boldEnd = "</strong>"
        }
        users = users + "<div class='gridItem h6'>" + bold + you + userName + boldEnd + "</div>";
    });
    $('#userList').html(users);
}

function displayMessages(chatHistory) {

    var historyString = "";
    var bold, boldEnd;


    chatHistory.forEach(message => {

        // console.log(message);
        //if user = this user
        //bold = <strong>
        //else
        // console.log(message.newName, myUserName, message.userName);

        if (message.newName === myUserName || myUserName === message.userName) {
            bold = "<strong>";
            boldEnd = "</strong>";
        }
        else {
            bold = "";
            boldEnd = "";
        }

        if (message.type === "chat") {

            var proccessedChat = proccessChat(message.message);
            historyString = historyString + "<div class='mr-3 ml-3 message h5' style='color:" + message.userColor + ";'>" + bold + message.userName + ": " + proccessedChat + " ";
            historyString = historyString + boldEnd + "<small class='timestamp h6'>" + message.timestamp + "</small></div>\n";
            // $('#messages').append($('<li>').text(msg));
        }
        if (message.type === "Name Update") {

            historyString = historyString + "<div class='mr-3 ml-3 message h5' style='color:black'>" + bold + message.oldName + " changed thier name to " + message.newName + boldEnd + "<small class='timestamp h6'> " + message.timestamp + "</small></div>\n";
        }
        if (message.type === "Set Name") {

            historyString = historyString + "<div class='mr-3 ml-3 message h5' style='color:black'>" + bold + message.newName + " has joined the chat" + boldEnd + "<small class='timestamp h6'> " + message.timestamp + "</small></div>\n";
        }

        if (message.type === "Color Change") {

            historyString = historyString + "<div class='mr-3 ml-3 message h5' style=color:" + message.color + ">" + bold + message.userName + " has changed thier colour to " + message.color + boldEnd + "<small class='timestamp h6'> " + message.timestamp + "</small></div>\n";
        }
        if (message.type === "Disconnect") {

            historyString = historyString + "<div class='mr-3 ml-3 message h5' style='color:black'>" + message.userName + " has disconnected <small class='timestamp h6'> " + message.timestamp + "</small></div>\n";
        }
    });

    $('#messages').html(historyString);
    document.getElementById('messages').scrollIntoView(false);
}

$(function () {
    var socket = io();
    $('form').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('message', $('#m').val());
        $('#m').val('');
        return false;
    });
    socket.on("New Connection", function (payload) {
    }
    );
    socket.on("Update Username", function (newUsername, chatHistory) {
        document.cookie = "username=" + newUsername + ";";
        console.log(document.cookie)
        myUserName = newUsername;
        displayMessages(chatHistory);
    }
    );
    socket.on("Update Colour", function (newColor, chatHistory) {
        document.cookie = "color=" + newColor + ";";
        myColor = newColor;
        displayMessages(chatHistory);
    })

    socket.on('Update Message Log', function (history) {
        displayMessages(history);
    });
    socket.on('Update User List', function (payload) {
        // console.log(payload);
        updateUserList(payload);
    });
    socket.on('setColorCookie', function (newColour) {
        document.cookie = "color=" + newColour + ";";
    });
    socket.on("Error", function (errorMessage) {
        $('#messages').prepend("<div class='mr-3 ml-3 message h5' style='color:red'>" + errorMessage + "</div>");
        document.getElementById('messages').scrollIntoView(false);
    });
});